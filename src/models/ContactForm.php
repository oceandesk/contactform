<?php

namespace oceandesk\contactform;

use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model
{
    protected $table = "contactform";
    public $timestamps = true;
}
